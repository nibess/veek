
<?php 

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration {

	public function up()
	{
	  Schema::create('user', function(Blueprint $table)
	  {
	      $table->increments('id');
	      $table->string('name',30)->nullable();
	      $table->string('email',60);
	      $table->timestamps();
	  });
	}
	public function down()
	    {
	        Schema::drop('user');
	    }

}
?>