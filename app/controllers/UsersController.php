<?php

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UsersController extends BaseController {



	public function index()
	{	
		$users = User::all();

		 if(!$users) {
            return response()->json([
                'message'   => 'Nenhum usuário cadastrado no momento.',
            ], 404);
        }
        return response()->json($users);
		
	}

/*############### Lista o usuario pelo ID  ##############*/
	public function UserUnico($id)
    {
        $user = User::find($id);

        if(!$user) {
            return response()->json([
                'message'   => 'Nenhum usuário encontrado com esse ID.',
            ], 404);
        }

        return response()->json($user);
    }
/*############### end Lista o usuario pelo ID  ##############*/

/*############### Criar usuario  ##############*/
    public function create(Request $request)
    {
        $user = new Users();
        $user->fill($request->all());
        $user->save();

        return response()->json($user);
    }
/*############### end Criar usuario  ##############*/

/*############### editar usuario  ##############*/
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if(!$user) {
            return response()->json([
                'message'   => 'Usuário não encontrado',
            ], 404);
        }

        $user->fill($request->all());
        $user->save();

        return response()->json($user);
    }
/*############### fim editar usuario  ##############*/

/*############### deletar usuario  ##############*/
    public function delete($id)
    {
        $user = User::find($id);

        if(!$user) {
            return response()->json([
                'message'   => 'Usuário não encontrado',
            ], 404);
        }

        $user->delete();
    }

/*############### fim deletar usuario  ##############*/

}
